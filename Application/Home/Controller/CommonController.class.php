<?php

namespace Home\Controller;
use Think\Controller;


defined("IN_APP")  or exit("Access Deny");
class CommonController extends Controller{
        
        private $user ; 
        static  $loginFail = 0 ; 
        /*
         * 公共登入页面
         */
        function Login(){
        		$this->assign('forget' , U('Common/forget'));
        		$this->assign('register' , U('Common/register'));
                $this->display();
        }
        
        /*
         * AJAX登录
         */
        function dologin(){
            if(IS_AJAX){
                $data = array() ; 
                $data["username"] = empty(I("post.username")) ? "" : I("post.username");
                $data["password"] = empty(I("post.password")) ? "" : md5(I("post.password"));
                 empty($res=M("User")->where($data)->select())
                ? exitJson(0, L("EaccountAndPass")) 
                : exitJson(1,"",setLoginSession($res[0]));
            }
        }
        
        
        
        
        /*
         * 检查登入状态
         * @param $username $password
         * @retrun bool  
         */
        function checkLogin(){
            $username = empty($_POST["username"]) ? false : trim($_POST["username"]);
            $password  = empty($_POST["password"]) ? false : trim($_POST["password"]);
            
            if(!username || !$password){
                exit(json_encode(array("status"=>"0","msg"=>"错误"))); //登入失败
            }
            
            $userModel = M("User");
            $userModel->dologin(array("username"=>$username , "password" => $password));
            
        }
    	
    	/*
    	 * 注册函数
    	 */
    	 function register() {
    	 	
    		$this->display();
    	 }
    	 
    	 
    
    
    
    
    
    
}