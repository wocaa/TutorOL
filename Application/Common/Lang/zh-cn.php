<?php
defined("IN_APP")  or exit("Access Deny");
/*
 * 公共语言包目录
 */
return array(
    "loginTitle"  =>  '登录',
    "defaultUser" =>"请输入手机号",
    "defaultPass"  =>"请输入密码",
    "login"             =>"登录" ,
    "forgetPass" =>"忘记密码" ,
    "newUser"   =>"新用户注册",
    "nickname"		=>"昵称",
    "password2" 	=> "请输入确认密码",
    "registerbtn"	=> "注册" , 
    "backToLogin"	=>	"返回登陆",
    
    //错误信息
    "EaccountAndPass" =>"账号或密码错误" , 
    
    
    
);


