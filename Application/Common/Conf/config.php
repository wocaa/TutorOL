<?php

defined("IN_APP")  or exit("Access Deny");

return array(
    
      //缓存
    'HTML_CACHE_ON'          =>    false,
    'TMPL_CACHE_ON'          =>   false, 
    'TMPL_CACHE_ON'          =>  false,   
    //主题
     'DEFAULT_THEME'         =>    'Default' ,
    //模板定界符
     'TMPL_L_DELIM'          =>    '{',
    'TMPL_R_DELIM'           =>    '}',
    //页面调试page_trace
     'SHOW_PAGE_TRACE'       =>true, 
     //语言包
    'LANG_SWITCH_ON'         =>     true,       //开启语言包功能        
    'LANG_AUTO_DETECT'       =>     true,      // 自动侦测语言 开启多语言功能后有效
    "DEFAULT_LANG"           =>       "zh-cn",
    
    
    //数据库配置
    'DB_TYPE'                =>  'mysql',     // 数据库类型
    'DB_HOST'                =>  '127.0.0.1', // 服务器地址
    'DB_NAME'                =>  'tutor',          // 数据库名
    'DB_USER'                =>  'root',      // 用户名
    'DB_PWD'                 =>  'admin',          // 密码
    'DB_PORT'                =>  '3306',        // 端口
    'DB_PREFIX'              =>  'nt_',    // 数据库表前缀 
    'DB_DEBUG'               =>  TRUE, // 数据库调试模式 开启后可以记录SQL日志
    'DB_FIELDS_CACHE'        =>  false,        // 启用字段缓存
    'DB_CHARSET'             =>  'utf8',      // 数据库编码默认采用utf8
    

);