<?php

/*
 * AJAX返回状态码函数
 * @param   string $status 
 * @param   string $msg
 * @return    json 推出系统
 */
function exitJson($status , $msg='',$call=''){
    if(function_exists($call)){
        call_user_func($call);
    }
  return  exit(json_encode(array("status"=>$status ,"msg"=>$msg))) ; 
}


/*
 * 设置登录以后的SESSION
 * @param   =>登录成功以后的查询数据
 */

function setLoginSession($param){
//    ["id"] => string(1) "1"
//    ["username"] => string(11) "15179168911"
//    ["password"] => string(32) "670b14728ad9902aecba32e22fa4f6bd"
//    ["nickname"] => string(6) "张三"
//    ["typeid"] => string(1) "0"
//    ["email"] => string(16) "735416909@qq.com"
//    ["grade"] => string(1) "1"
//    ["address"] => string(12) "江西财大"
//    ["sex"] => string(1) "0"
//    ["power"] => string(1) "1"
//    ["identification"] => string(1) "0"
//    ["ret_time"] => string(9) "111111111"
    session("uid",$param["id"]);
    unset($param["password"]);
    session("userinfo",$param);
    if(!empty($param["nickname"]))
        {
                defined("__USERNAME__") or define("__USERNAME__" , $param["username"]);
                 defined("__UID__") or define("__UID__" , $param["id"]);
        } 
}

/*
 * 登录控制函数
 * 访问控制已经登录过的用户没退出之前不能再登录
 * 跳转到主页  或者自定义页面
 */

function loginControl($url = ''){
//      if(!empty($url)) 
//      {
//          defined("__UID__") or redirect($url); 
//      }
//      else
//      {
//             defined("__UID__") or redirect(__SITE__);  
//      }
//   
   
}

//获取IP地址
function getIPaddress()

{

    $IPaddress='';

    if (isset($_SERVER)){

        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])){

            $IPaddress = $_SERVER["HTTP_X_FORWARDED_FOR"];

        } else if (isset($_SERVER["HTTP_CLIENT_IP"])) {

            $IPaddress = $_SERVER["HTTP_CLIENT_IP"];

        } else {

            $IPaddress = $_SERVER["REMOTE_ADDR"];

        }

    } else {

        if (getenv("HTTP_X_FORWARDED_FOR")){

            $IPaddress = getenv("HTTP_X_FORWARDED_FOR");

        } else if (getenv("HTTP_CLIENT_IP")) {

            $IPaddress = getenv("HTTP_CLIENT_IP");

        } else {

            $IPaddress = getenv("REMOTE_ADDR");

        }

    }

    return $IPaddress;

}